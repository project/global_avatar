<?php
/**
 * @file
 * Administrative settings callbacks for the Global Avatar module.
 */

/**
 * Administrative settings for the Global Avatar module.
 */
function global_avatar_admin_settings() {
  $form = array();

  $form['global_avatar_file_system_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Global avatar file system path'),
    '#default_value' => variable_get('global_avatar_file_system_path', GLOBAL_AVATAR_DEFAULT_FILE_SYSTEM_PATH),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#after_build' => array('system_check_directory'),
  );

  return system_settings_form($form);
}

