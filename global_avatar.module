<?php

/**
 * @file
 * Allows users to set a global avatar in multisite environments where the user
 * table is shared.
 */

define('GLOBAL_AVATAR_DEFAULT_FILE_SYSTEM_PATH', 'public://global_avatar');

/**
 * Implements hook_menu().
 */
function global_avatar_menu() {
  $items = array();

  $items['admin/config/people/global-avatar'] = array(
    'title' => 'Global avatar',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('global_avatar_admin_settings'),
    'access arguments' => array('administer users'),
    'file' => 'global_avatar.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_form_[form_id]_alter().
 *
 * @see user_edit_form()
 */
function global_avatar_form_user_profile_form_alter(&$form, &$form_state) {
  // If variable_get('user_pictures', 0) is 0 or the $register param in user_edit_form() is TRUE, the 'picture' key will not exist.
  if (isset($form['picture'])) {
    $avatar = global_avatar_get_user_avatar($form_state['user']->mail);
    if (!empty($avatar) && $form['picture']['picture_delete']['#access'] == FALSE) {
      $form['picture']['picture_delete_global_avatar'] = $form['picture']['picture_delete'];
      $form['picture']['picture_delete_global_avatar']['#access'] = TRUE;

      $form['picture']['picture_current']['#weight'] = 0;
      $form['picture']['picture_delete_global_avatar']['#weight'] = 1;
      $form['picture']['picture_upload']['#weight'] = 2;
    }

    $form['#validate'][] = 'global_avatar_validate_picture';
  }
}

/**
 * Moves the image to the global avatar location.
 *
 * @param $form
 *   A nested array of form elements that comprise the form.
 * @param &$form_state
 *   A keyed array containing the current state of the form.
 *
 * @return void NULL
 */
function global_avatar_validate_picture($form, &$form_state) {
  // Makes sure an image was uploaded and there weren't any errors.
  if (!empty($form_state['values']['picture_upload']) && !form_get_error($form['picture']['picture_upload'])) {
    $path = variable_get('global_avatar_file_system_path', GLOBAL_AVATAR_DEFAULT_FILE_SYSTEM_PATH);

    $new_avatar = file_unmanaged_copy($form_state['values']['picture_upload']->uri, $path . '/' . $form_state['values']['picture_upload']->filename, FILE_EXISTS_RENAME);

    if (!empty($new_avatar)) {
      global_avatar_clear_by_mail($form_state['user']->mail);

      db_insert('global_avatar')
        ->fields(array(
          'mail' => $form_state['user']->mail,
          'picture_path' => $new_avatar,
        ))
        ->execute();
    }
    else {
      form_set_error('picture_upload', t(
        "Failed to copy the picture image to the global avatar directory; the %directory directory doesn't exist or is not writable.",
        array('%directory' => $path)
      ));
    }
  }
  elseif (!empty($form_state['values']['picture_delete']) || !empty($form_state['values']['picture_delete_global_avatar'])) {
    global_avatar_clear_by_mail($form_state['user']->mail);
  }
}

/**
 * Process variables for user-picture.tpl.php. This function overrides variables set in
 * template_preprocess_user_picture().
 *
 * @param $variables
 *   An array contains the following arguments: $account
 *
 * @see user-picture.tpl.php
 * @see template_preprocess_user_picture()
 */
function global_avatar_preprocess_user_picture(&$variables) {
  if (variable_get('user_pictures', 0)) {
    $account = $variables['account'];

    if (!empty($account->mail)) {
      $mail = $account->mail;
    }
    elseif (!empty($variables['user']->mail)) {
      $mail = $variables['user']->mail;
    }
    else {
      $mail = '';
    }

    $avatar = global_avatar_get_user_avatar($mail);

    if (!empty($avatar)) {
      $path = $avatar;
    }
    elseif (!empty($account->picture->uri)) {
      $path = $account->picture->uri;
    }
    elseif (variable_get('user_picture_default', '')) {
      $path = variable_get('user_picture_default', '');
    }

    if (!empty($path) && $account->uid > 0) {
      $alt = t("@user's picture", array('@user' => $account->name ? $account->name : variable_get('anonymous', t('Anonymous'))));

      if (!empty($variables['user_picture_style'])) {
        $style = $variables['user_picture_style'];
      }
      else {
        $style = variable_get('user_picture_style', '');
      }

      if (!empty($style)) {
        $variables['user_picture'] = $variables['picture'] = theme(
          'image_style',
          array('style_name' => $style, 'path' => $path, 'alt' => $alt, 'title' => $alt)
        );
      }
      else {
        $variables['user_picture'] = $variables['picture'] = theme(
          'image',
          array('path' => $path, 'alt' => $alt, 'title' => $alt)
        );
      }

      if (!empty($account->uid) && user_access('access user profiles')) {
        $attributes = array('attributes' => array('title' => t('View user profile.')), 'html' => TRUE);
        $variables['picture'] = l($variables['picture'], "user/$account->uid", $attributes);
      }
    }
  }
}

/**
 * Respond to user deletion.
 *
 * @param $account
 *   The account that is being deleted.
 *
 * @see user_delete_multiple()
 */
function global_avatar_user_delete($account) {
  global_avatar_clear_by_mail($account->mail);
}

/**
 * Returns the path to user's avatar.
 *
 * @param $mail
 *   Email address of a user.
 *
 * @return string
 *   Path to user's avatar or empty string.
 */
function global_avatar_get_user_avatar($mail) {
  return db_query('SELECT picture_path FROM {global_avatar} WHERE mail = :mail', array(':mail' => $mail))->fetchField();
}

/**
 * Clears avatar by email: removes record from table and removes file.
 *
 * @param $mail
 */
function global_avatar_clear_by_mail($mail) {
  $avatar = global_avatar_get_user_avatar($mail);
  file_unmanaged_delete($avatar);
  db_delete('global_avatar')->condition('mail', $mail)->execute();
}

/**
 * Implements hook_views_data_alter().
 *
 * @param $data
 *   An array of all Views data, passed by reference. See hook_views_data() for
 *   structure.
 *
 * @see hook_views_data()
 */
function global_avatar_views_data_alter(&$data) {
  //dsm($data);
  $data['users']['picture']['field']['handler'] = 'views_handler_field_user_global_avatar_picture';
}

/**
 * Class views_handler_field_user_global_avatar_picture
 */
class views_handler_field_user_global_avatar_picture extends views_handler_field_user_picture {
  function render($values) {
    if ($this->options['image_style'] && module_exists('image')) {
      $avatar = global_avatar_get_user_avatar($values->users_mail);

      if (!empty($avatar)) {
        $picture_filepath = $avatar;
      }
      elseif ($picture_fid = $this->get_value($values)) {
        $picture = file_load($picture_fid);
        $picture_filepath = $picture->uri;
      }
      else {
        $picture_filepath = variable_get('user_picture_default', '');
      }

      if (file_valid_uri($picture_filepath)) {
        $output = theme('image_style', array('style_name' => $this->options['image_style'], 'path' => $picture_filepath));
        if ($this->options['link_photo_to_profile'] && user_access('access user profiles')) {
          $uid = $this->get_value($values, 'uid');
          $output = l($output, "user/$uid", array('html' => TRUE));
        }
      }
      else {
        $output = '';
      }
    }
    else {
      // Fake an account object.
      $account = new stdClass();
      if ($this->options['link_photo_to_profile']) {
        // Prevent template_preprocess_user_picture from adding a link by not setting the uid.
        $account->uid = $this->get_value($values, 'uid');
      }
      $account->name = $this->get_value($values, 'name');
      $account->mail = $this->get_value($values, 'mail');
      $account->picture = $this->get_value($values);
      $output = theme('user_picture', array('account' => $account));
    }

    return $output;
  }
}

